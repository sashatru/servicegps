package com.weboff.servicegps;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.GpsStatus.Listener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class MyService extends Service implements LocationListener {

    MyLocation myLocation;

    int satelity = 0;
    String satelitySend;

    private static int NOTIFICATION_ID = 2;
    String formatedTime;


    int mMount;
    int mDate;
    int mDay;
    int mHour;
    int mMilis;
    int mMinute;
    int mSecond;
    int mYear;


    int idtest;

    String TimeoutGetGetDataInSeconds;
    String TimeoutSendDataInSeconds;
    String TimeoutSendLog;
    String TimeoutSendPing;

    String DeviceLogin;
    String DevicePswd;

    String SrvFTP;
    String HttpLink;


    String battaryS;

    String globalFile = "";

    int statusGPS;


    LocationManager lManager;


    boolean UseSNinsteadOfLogin;
    boolean enableGPS;

    int percent = 0;


    float getSpeed;

    long getTime;

    float getBearing;

    float getAccuracy;

    String getProvider;

    double getAltitude;


    String getDate;


    private Context cntx = null;

    private final static String FILENAME = "/log.txt";


    TelephonyManager Tel;
    MyPhoneStateListener MyListener;

    MainActivity my = new MainActivity();
    private NotificationManager mNM;
    private int NOTIFICATION = 1;

    final String LOG_TAG = "myServiceLogs";

    boolean stopThread = false;


    boolean checkData = false;


    String device_idG;

    double latitudeNetwork;
    double longitudeNetwork;


    double latitudeGPS = 0;
    double longitudeGPS = 0;


    DBHelper dbHelper;


    LocationManager locMgr;


    public class LocalBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return mBinder;
    }


    private final IBinder mBinder = new LocalBinder();


    private void updateDate() {
        Calendar c = Calendar.getInstance();
        mMount = c.get((Calendar.MONTH));
        mDate = c.get((Calendar.DATE));
        mDay = c.get((Calendar.DAY_OF_MONTH));
        mHour = c.get((Calendar.HOUR));
        mMinute = c.get((Calendar.MINUTE));
        mSecond = c.get((Calendar.SECOND));
        mYear = c.get((Calendar.YEAR));
        mMilis = c.get(Calendar.MILLISECOND);
    }

    private void openFile(String fileName) {
        try {

            InputStream inStream = new FileInputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + FILENAME);

            if (inStream != null) {
                InputStreamReader tmp =
                        new InputStreamReader(inStream);
                BufferedReader reader = new BufferedReader(tmp);
                String str;
                StringBuffer buffer = new StringBuffer();

                while ((str = reader.readLine()) != null) {
                    buffer.append(str + "\n");
                }

                inStream.close();

                //	Toast.makeText(getApplicationContext(),
                //			"file = : " + buffer.toString(), Toast.LENGTH_LONG)
                //			.show();
                globalFile = buffer.toString();
                //mEdit.setText(buffer.toString());
            }
        } catch (Throwable t) {
            //	Toast.makeText(getApplicationContext(),
            //	"Exception2: " + t.toString(), Toast.LENGTH_LONG)
            //	.show();
        }
    }


    private void saveFile(String FileName, String save) {
        try {

            String fil = Environment.getExternalStorageDirectory().getAbsolutePath();


            Log.d(LOG_TAG, "fill===" + fil);

            OutputStreamWriter outStream =
                    new OutputStreamWriter(new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath() + FILENAME));
            outStream.write(globalFile + save);
            outStream.close();
        } catch (Throwable t) {
            //	Toast.makeText(getApplicationContext(),
            //"Exception1: " + t.toString(), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onCreate() {

        //imei телефона
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            telephonyManager.getDeviceId();
        }
        String dev = telephonyManager.getDeviceId();


        //отключения сплящего режима
        PowerManager mgr = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        WakeLock wakeLock = null;
        if (mgr != null) {
            wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "serviceGPS:MyWakeLock");
        }
        wakeLock.acquire(10 * 60 * 1000L /*10 minutes*/);


        Log.d(LOG_TAG, "service created TMS.GPSensor ");
        Toast.makeText(this, "service created TMS.GPSensor", Toast.LENGTH_SHORT).show();
        my.check = 0;


        cntx = this.getBaseContext();


        //satelite
        locMgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locMgr.addGpsStatusListener(onGpsStatusChange);

        onGpsStatusChange.onGpsStatusChanged(GpsStatus.GPS_EVENT_SATELLITE_STATUS);


        //  Toast.makeText(getApplicationContext(), "111111111 " +    String.valueOf(scale), Toast.LENGTH_SHORT).show();


        MyListener = new MyPhoneStateListener();
        Tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        Tel.listen(MyListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);


        //

        TelephonyManager telephonyManager2 = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager2.getDeviceId();


        device_idG = telephonyManager.getDeviceId();
        //	Log.d(LOG_TAG,"device_id = " +device_id);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDateString = dateFormat.format(new Date());

        updateDate();
        openFile(FILENAME);
        saveFile(FILENAME, formattedDateString + "   create servis - OK");


        //saveFile(FILENAME, mYear + "-" + mMount + "-" + mDay + "_" + mHour + ":" + mMinute + ":" + mSecond + ":" + mMilis + "create servis - OK");
        openFile(FILENAME);
        saveFile(FILENAME, formattedDateString + "   running servis - OK");


        stopThread = false;


        //setting
        SharedPreferences mSettings;
        mSettings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        Editor editor2 = mSettings.edit();
        editor2.commit();


        if (mSettings.contains("TimeoutSendDataInSecondsP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            // Toast.makeText(this, "Text load = "+ mSettings.getString("TimeoutSendDataInSecondsP", "")  , Toast.LENGTH_SHORT).show();

            TimeoutSendDataInSeconds = mSettings.getString("TimeoutSendDataInSecondsP", "");
        }

        if (mSettings.contains("TimeoutSendLogP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            TimeoutSendLog = mSettings.getString("TimeoutSendLogP", "");
            // Toast.makeText(this, "Text load = "+ mSettings.getString("TimeoutSendLogP", "")  , Toast.LENGTH_SHORT).show();
        }
        if (mSettings.contains("TimeoutSendPingP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            // Toast.makeText(this, "Text load = "+ mSettings.getString("TimeoutSendPingP", "")  , Toast.LENGTH_SHORT).show();

            TimeoutSendPing = mSettings.getString("TimeoutSendPingP", "");
        }

        if (mSettings.contains("DeviceLoginP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            //    Toast.makeText(this, "Text load = "+ mSettings.getString("DeviceLoginP", "")  , Toast.LENGTH_SHORT).show();
            DeviceLogin = mSettings.getString("DeviceLoginP", "");
        }

        if (mSettings.contains("DevicePswdP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            //  Toast.makeText(this, "Text load = "+ mSettings.getString("DevicePswdP", "")  , Toast.LENGTH_SHORT).show();
            DevicePswd = mSettings.getString("DevicePswdP", "");
        }


        if (mSettings.contains("SrvFTPP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            //   Toast.makeText(this, "Text load = "+ mSettings.getString("SrvFTPP", "")  , Toast.LENGTH_SHORT).show();
            SrvFTP = mSettings.getString("SrvFTPP", "");
        }
        if (mSettings.contains("HttpLinkP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            //   Toast.makeText(this, "Text load = "+ mSettings.getString("HttpLinkP", "")  , Toast.LENGTH_SHORT).show();
            HttpLink = mSettings.getString("HttpLinkP", "");
        }

        //Boolean UseSNinsteadOfLoginP;
        if (mSettings.contains("UseSNinsteadOfLoginP")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));

            UseSNinsteadOfLogin = mSettings.getBoolean("UseSNinsteadOfLoginP", false);
            // Toast.makeText(this, "Text load = "+ mSettings.getBoolean("UseSNinsteadOfLoginP", "")  , Toast.LENGTH_SHORT).show();
            //	UseSNinsteadOfLoginP = mSettings.getBoolean("UseSNinsteadOfLoginP", login);
            //getBoolean("UseSNinsteadOfLoginP", "");
        }


        //Boolean UseSNinsteadOfLoginP;
        if (mSettings.contains("enableGPS")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));

            enableGPS = mSettings.getBoolean("enableGPS", true);
            // Toast.makeText(this, "Text load = "+ mSettings.getBoolean("UseSNinsteadOfLoginP", "")  , Toast.LENGTH_SHORT).show();
            //	UseSNinsteadOfLoginP = mSettings.getBoolean("UseSNinsteadOfLoginP", login);
            //getBoolean("UseSNinsteadOfLoginP", "");
        }


        if (mSettings.contains("TurnOffGpsAfterSend")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            // Toast.makeText(this, "Text load = "+ mSettings.getBoolean("UseSNinsteadOfLoginP", "")  , Toast.LENGTH_SHORT).show();
            //	UseSNinsteadOfLoginP = mSettings.getBoolean("UseSNinsteadOfLoginP", login);
            //getBoolean("UseSNinsteadOfLoginP", "");
        }


        if (mSettings.contains("SendGPSDataAtRealTime")) {
            //   nicknameText.setText(mSettings.getString("mysettings", ""));
            // Toast.makeText(this, "Text load = "+ mSettings.getBoolean("UseSNinsteadOfLoginP", "")  , Toast.LENGTH_SHORT).show();
            //	UseSNinsteadOfLoginP = mSettings.getBoolean("UseSNinsteadOfLoginP", login);
            //getBoolean("UseSNinsteadOfLoginP", "");
        }


        if (mSettings.contains("ServerConnectionRetryTimeP")) {

        }
        if (mSettings.contains("WaitServerAnswerP")) {

        }
        if (mSettings.contains("TimeoutGetGetDataInSecondsP")) {


            TimeoutGetGetDataInSeconds = mSettings.getString("TimeoutGetGetDataInSecondsP", "");

        }
        if (mSettings.contains("LogSizeKbP")) {

        }
        if (mSettings.contains("LogFileCountP")) {

        }

//оповещение
        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        showNotification();


        //	 someTask();
		 
		/*	LocationManager lm =
		              (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		       lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		
		       
		       lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		       */


        LocationListener locListener = new LocationListener() {
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                //Log.d(LT,"onStatusChanged "+provider);
            }

            @Override
            public void onProviderEnabled(String provider) {
                //   Log.d(LT,"onProviderEnabled "+provider);
            }

            @Override
            public void onProviderDisabled(String provider) {
                // Log.d(LT,"onProviderDisabled "+provider);
            }

            @Override
            public void onLocationChanged(Location location) {

            }
        };
//Связываем менеджер и сервис
        lManager = null;
        lManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        //Назначаем слушателя на возможные провайдеры по очереди
        //(для верности завернем их в try, т.к. некоторые могут быть недоступны)
        try {
            lManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locListener);
//           Toast.makeText(this, "GPS_PROVIDER", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
        try {
            lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 10, locListener);
//           Toast.makeText(this, "NETWORK_PROVIDER", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
        try {
            lManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 10, locListener);
//           Toast.makeText(this, "PASSIVE_PROVIDER", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }

        someTask();
        sendData();

        //   ping();
        //    logs();

        //     mPlayer = MediaPlayer.create(this, R.raw.psy);
        //	mPlayer.setLooping(false);
        myLocation = new MyLocation(MyService.this);

    }

    //показать оповещение
    private void showNotification() {
        Notification notification = new Notification(R.drawable.ic_launcher, "Запуск службы TMS.GPSensor",
                System.currentTimeMillis());
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        notification.setLatestEventInfo(this, getText(R.string.app_name),
                "Служба запущена TMS.GPSensor ", contentIntent);
        mNM.notify(NOTIFICATION, notification);
    }


    //старт команд
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "MyService onStartCommand Received start id " + startId + ": " + intent);


        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();

        Notification notification = new android.app.Notification(icon, "Приложение запущено", when);
        //Создание намерения с указанием класса вашей Activity, которую хотите вызвать при           нажатии на оповещение.
        Intent notificationIntent = new Intent(this, MyService.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.setLatestEventInfo(this, "Системный процес", "TMS.GPSensor", contentIntent);


        startForeground(NOTIFICATION_ID, notification);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.

        //       Intent intent2= new Intent("android.location.GPS_ENABLED_CHANGE");
        //       intent2.putExtra("enabled", true);
        //       sendBroadcast(intent2);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDateString = dateFormat.format(new Date());


        updateDate();
        openFile(FILENAME);
        saveFile(FILENAME, formattedDateString + "   onStartCommand - OK");

        myLocation.startPreparations();

        Log.d(LOG_TAG, "start command11111");
        return START_STICKY;
    }


    //получение координат и отправка на сервер
    void someTask() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDateString = dateFormat.format(new Date());

        updateDate();
        openFile(FILENAME);
        saveFile(FILENAME, formattedDateString + "   create thread getData - OK");

        //saveFile(FILENAME, mYear + "-" + mMount + "-" + mDay + "_" + mHour + ":" + mMinute + ":" + mSecond + ":" + mMilis + "getData x y - OK");

        PowerManager mgr = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        WakeLock wakeLock = null;
        if (mgr != null) {
            wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "serviceGPS:MyWakeLock");
        }
        wakeLock.acquire(10 * 60 * 1000L /*10 minutes*/);


        Log.d(LOG_TAG, "Task");
        new Thread(() -> {

            int cou = 0;
            while (cou == 0) {

                // for (int i = 0; i<=4; i++) {
                //   Log.d(LOG_TAG, "iT =--- " + i);


                //    Log.d(LOG_TAG,"i======" + i);

                //       toast(i);


                if (stopThread) break;


                //  stopSelf();


                try {

                    int val;

                    try {
                        val = Integer.parseInt(TimeoutGetGetDataInSeconds);


                    } catch (NumberFormatException e) {
                        //e.printStackTrace();
                        val = 0;
                    }


                    updateDate();


                    openFile(FILENAME);

                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    String formattedDateString1 = dateFormat1.format(new Date());

                    saveFile(FILENAME, formattedDateString1 + "   running thread getData - OK");


                    recBD();


                    TimeUnit.SECONDS.sleep(val);


                    //   postData();


                } catch (InterruptedException e) {
                    //    e.printStackTrace();

                    String str = "";
                    for (int i = 0; i < e.getStackTrace().length; i++) {
                        str += e.getStackTrace()[i].toString();
                    }
                }
            }
            stopSelf();
        }).start();


    }


    void recBD() {
        try {
            if (enableGPS)  //если включена галочка только GPS
            {
                recGPS();
            } else// если выключена галочка "Только GPS"
            {
                recALL();// если выключена галочка "Только GPS", тогда брать gps если нет GPS, брать соту
            }
        } catch (Exception ignored) {
        }
    }
	 
	 
	 
	 
	 
	/* private Location getLocationByProvider(String provider) {
		    Location location = null;
		
		    LocationManager locationManager = (LocationManager) getApplicationContext()
		            .getSystemService(Context.LOCATION_SERVICE);
		    try {
		        if (locationManager.isProviderEnabled(provider)) {
		            location = locationManager.getLastKnownLocation(provider);
		        }
		    } catch (IllegalArgumentException e) {
		        //Log.d(TAG, "Cannot acces Provider " + provider);
		    }
		    return location;
		}
	 
	 */


    void recGPS()// если включена галочка только GPS
    {
        LocationManager locationManager = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);


        Location gpslocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        // if we have only one location available, the choice is easy


        if (gpslocation != null) {
            // Log.d(TAG, "No GPS Location available.");


            dbHelper = new DBHelper(this);


            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();

            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Context context = this.getApplicationContext();
            Intent batteryStatus = context.registerReceiver(null, ifilter);
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);


            double lat = gpslocation.getLatitude();
            double lon = gpslocation.getLongitude();

            getBearing = gpslocation.getBearing();
            getSpeed = gpslocation.getSpeed();
            getAccuracy = gpslocation.getAccuracy();
            getAltitude = gpslocation.getAltitude();

            String val;
            try {
                val = Integer.toString(level);
                // Log.d(LOG_TAG,"нормуль ===---------    val ");
            } catch (NumberFormatException e) {

                // Log.d(LOG_TAG,"ошибка === " );
                val = "0";
            }

            //   percent = level;
            battaryS = val;


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDateString = dateFormat.format(new java.util.Date());
            cv.put("latitude", lat);
            cv.put("longitude", lon);
            cv.put("getBearing", getBearing);
            cv.put("getSpeed", getSpeed);
            cv.put("getBat", battaryS);
            cv.put("getAccuracy", getAccuracy);

            cv.put("getSat", satelity);
            cv.put("getAltitude", getAltitude);
            cv.put("getDate", formattedDateString);
            // cv.put("getDate", mYear + "-" + mMount + "-" + mDay + "%20" + mHour + ":" + mMinute + ":" + mSecond + ":" + mMilis);
            //  cv.put("test","test");


            // вставлЯем запись и получаем ее ID
            long rowID = db.insert("mytable", null, cv);

        }
    }

    void recALL()// если выключена галочка "Только GPS", тогда брать gps если нет GPS, брать соту
    {
//new location ====================================================================================
        Location actualLocation = myLocation.getActualLocation();

        if (actualLocation != null) {
            dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues cv = new ContentValues();

            IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Context context = this.getApplicationContext();
            Intent batteryStatus = context.registerReceiver(null, ifilter);
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

            double lat = actualLocation.getLatitude();
            double lon = actualLocation.getLongitude();
            getBearing = actualLocation.hasBearing() ? actualLocation.getBearing() : 0.0f;
            getSpeed = actualLocation.hasSpeed() ? actualLocation.getSpeed() : 0.0f;
            getAccuracy = actualLocation.hasAccuracy() ? actualLocation.getAccuracy() : 0.0f;
            getAltitude = actualLocation.hasAltitude() ? actualLocation.getAltitude() : 0.0f;

            String val;
            try {
                val = Integer.toString(level);
            } catch (NumberFormatException e) {
                val = "0";
            }

            battaryS = val;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDateString = dateFormat.format(new Date());

            cv.put("latitude", lat);
            cv.put("longitude", lon);
            cv.put("getBearing", getBearing);
            cv.put("getSpeed", getSpeed);
            cv.put("getBat", battaryS);
            cv.put("getAccuracy", getAccuracy);

            cv.put("getSat", satelity);
            cv.put("getAltitude", getAltitude);
            cv.put("getDate", formattedDateString);

            // вставлЯем запись и получаем ее ID
            long rowID = db.insert("mytable", null, cv);
            Log.i(LOG_TAG, "ping coordinates  MyLocation.getActualCoordinates():" + actualLocation);
        }
//new location ====================================================================================
    }


    //старт службы
    @Override
    public void onStart(Intent intent, int startId) {

        Toast.makeText(this, "service starter TMS.GPSensor", Toast.LENGTH_SHORT).show();

        SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        dateFormat3.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDateString3 = dateFormat3.format(new Date());
        saveFile(FILENAME, formattedDateString3 + " START SERVIS - OK");
    }


    //удаление службы
    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "delete TMS.GPSensor");
        mNM.cancel(NOTIFICATION);

        updateDate();
        openFile(FILENAME);

        saveFile(FILENAME, mYear + "-" + mMount + "-" + mDay + "_" + mHour + ":" + mMinute + ":" + mSecond + ":" + mMilis + "delete servis - OK");

        my.check = 1;
        stopThread = true;


        this.stopForeground(false);
        Toast.makeText(this, "service stopped TMS.GPSensor", Toast.LENGTH_SHORT).show();
    }


    //получение координат
    @Override
    public void onLocationChanged(Location location) {

        double x = 0;
        double y = 0;


        latitudeGPS = x;
        longitudeGPS = y;


        if (location != null) {
            // Log.d(TAG, "пїЅРёСЂРѕС‚Р°="+location.getLatitude());
            // Log.d(TAG, "вЂћРѕР»РіРѕС‚Р°="+location.getLongitude());


            location.getProvider();
            if (LocationManager.GPS_PROVIDER.equals(location)) {
                latitudeGPS = location.getLatitude();
                longitudeGPS = location.getLongitude();
            }

            if (LocationManager.NETWORK_PROVIDER.equals(location)) {
                latitudeNetwork = location.getLatitude();
                longitudeNetwork = location.getLongitude();
            }


            SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            dateFormat3.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDateString3 = dateFormat3.format(new Date());

            saveFile(FILENAME, formattedDateString3 + " GPS ON - OK");


            statusGPS = 1;

            //  txt.setText("пїЅРёСЂРѕС‚Р°3333="+ location.getLatitude() + "вЂћРѕР»РіРѕС‚Р°3333=" + location.getLongitude());

            //      location.getLatitude();
            //  speed = location.getSpeed();
            //  bearing =  location.getBearing();


            //   x = location.getLatitude();
            //   y = location.getLongitude();


            // 	   latitudeGPS = x;
            // 	   longitudeGPS = y;


            SimpleDateFormat dateFormat0 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            dateFormat0.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDateString0 = dateFormat0.format(new Date());


            //	location.getTime();

            // String varTime="";
            // varTime=String.valueOf(location.getTime());
            Log.d(LOG_TAG, formattedDateString0);


            getSpeed = (location.getSpeed());

            getTime = (location.getTime());

            getBearing = (location.getBearing());

            getAccuracy = (location.getAccuracy());

            getProvider = (location.getProvider());

            getAltitude = location.getAltitude();


            //  Log.d(LOG_TAG," широта "+ location.getLatitude() + " долгота " + location.getLongitude());


            if (stopThread) {

            } else {
                //  Toast.makeText(this, " широта "+ location.getLatitude() + "долгота " + location.getLongitude() +  " B=  " + getBearing + "   a   =  " +  getAltitude, Toast.LENGTH_SHORT).show();
            }


            Log.d("id", "Координаты изменены");
        } else {
            //latitude =  0;
            //	longitude = 0;
   		
   		
   	/*	LocationManager   locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        Location location2 = locationManager
                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        
	if (location2 != null) {
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		Date date_now = cal.getTime();
		cal.setTimeInMillis(location2.getTime());
		Date date_loc = cal.getTime();
		
		
		getSpeed = (location2.getSpeed());

		 getTime  = (location2.getTime());
		
		 getBearing = (location2.getBearing());
		
		 getAccuracy = (location2.getAccuracy());
		
		 getProvider = (location2.getProvider());
		
		 getAltitude =  location2.getAltitude();
		
		
		latitude = location2.getLatitude();
		longitude = location2.getLongitude();
	} else {
		//Log.e(TAG, "location is null");
		*/

            latitudeGPS = 0;
            longitudeGPS = 0;
        }


    }


    //GPS
    @Override
    public void onProviderDisabled(String provider) {
        Log.d("id", "Модуль выключен");
        statusGPS = 0;
        Toast.makeText(this, "Модуль выключен", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("id", "Модуль включен");
        Toast.makeText(this, "Модуль включен", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("id", "Статус модуля");
        latitudeGPS = 0;
        longitudeGPS = 0;

        Toast.makeText(this, "Статус модуля", Toast.LENGTH_SHORT).show();
    }


    public void sendData() {
        //получени данных иz БД и запуск метода пост дата

        //potok


        new Thread(() -> {


            String x = null;
            String y = null;

            int cou = 0;
            while (cou == 0) {


                if (stopThread) break;

                try {

                    int val;


                    Log.d(LOG_TAG, "TimeoutSendDataInSeconds === " + TimeoutSendDataInSeconds);

                    try {
                        val = Integer.parseInt(TimeoutSendDataInSeconds);
                        Log.d(LOG_TAG, "нормуль === ");
                    } catch (NumberFormatException e) {
                        //e.printStackTrace();
                        Log.d(LOG_TAG, "ошибка === ");
                        val = 0;
                    }


                    ////----
                    updateDate();
                    openFile(FILENAME);


                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String formattedDateString = dateFormat.format(new Date());

                    saveFile(FILENAME, formattedDateString + "open select x y iz BD - OK");
                    //------


                    TimeUnit.SECONDS.sleep(val);
                    postData();

                    // Log.d(LOG_TAG, "PINGPOST=== " + i);
                    //---

                    //---


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            stopSelf();
        }).start();
    }


    //отправка координат
    public void postData() {


        checkData = false;
    	
    /*	
    	
    	Calendar c2 = Calendar.getInstance();
		 mMount = c2.get((Calendar.MONTH));
		 mDate = c2.get((Calendar.DATE));
		 mDay = c2.get((Calendar.DAY_OF_MONTH));
		 mHour = c2.get((Calendar.HOUR));
		 mMinute = c2.get((Calendar.MINUTE));
		 mSecond = c2.get((Calendar.SECOND));
		 mYear = c2.get((Calendar.YEAR));
		 mMilis = c2.get(Calendar.MILLISECOND);
	*/


        String x = null;
        String y = null;
        Log.d(LOG_TAG, "ПОСТ ДАТА СРАБОТАЛА ");


        updateDate();
        openFile(FILENAME);


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String formattedDateString = dateFormat.format(new Date());

        saveFile(FILENAME, formattedDateString + "   postData - send x y - STATUS");

        ContentValues cv = new ContentValues();

        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //чтение
        Log.d(LOG_TAG, "--- Rows in mytable: ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("mytable", null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернетсЯ false
        if (c.moveToFirst()) {

            // определЯем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int latitudeColIndex = c.getColumnIndex("latitude");
            int longitudeColIndex = c.getColumnIndex("longitude");
            int getBearingColIndex = c.getColumnIndex("getBearing");
            int getSpeedColIndex = c.getColumnIndex("getSpeed");
            int getBatColIndex = c.getColumnIndex("getBat");
            int getAccuracyColIndex = c.getColumnIndex("getAccuracy");

            int getSatColIndex = c.getColumnIndex("getSat");
            int getAltitudeColIndex = c.getColumnIndex("getAltitude");
            int getDateColIndex = c.getColumnIndex("getDate");


            do {
                // получаем значениЯ по номерам столбцов и пишем все в лог


                String bat = c.getString(getBatColIndex);
                //String bat = "111";

                getSpeed = c.getFloat(getSpeedColIndex);
                getBearing = c.getFloat(getBearingColIndex);
                getAltitude = c.getDouble(getAltitudeColIndex);
                getAccuracy = c.getFloat(getAccuracyColIndex);
                satelitySend = c.getString(getSatColIndex);
                getDate = c.getString(getDateColIndex);

                Log.d(LOG_TAG,
                        "ID = " + c.getInt(idColIndex) +
                                ", latitude = " + c.getString(latitudeColIndex) +
                                ", longitude = " + c.getString(longitudeColIndex) +
                                ", getSpeed = " + getSpeed +
                                ", getBearing" + getBearing +
                                ", getAltitude" + getAltitude +
                                ", getAccuracy" + getAccuracy +
                                ", getSat" + c.getString(getSatColIndex) + // +
                                ", getAltitude" + c.getString(getAltitudeColIndex) +
                                ", getDate" + c.getString(getDateColIndex));


                x = c.getString(latitudeColIndex);
                y = c.getString(longitudeColIndex);

                idtest = c.getInt(idColIndex);


                ///---
                //--
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = null;

                if (UseSNinsteadOfLogin == true) {
                    //httppost = new HttpPost(HttpLink + "?dt="+ mYear + "-" + mMount + "-" + mDay + "%20" + mHour + ":" + mMinute + ":" + mSecond +"&x=" + x + "&y=" + y + "&sp="+getSpeed +"&az="+getBearing+"&al=" + getAltitude+ "&bt=" + battaryS + "&st="+ satelitySend + "&lg=" + device_idG + "&pw=" + device_idG + "&g=" + percent);
                    httppost = new HttpPost(HttpLink + "?dt=" + getDate + "&x=" + x + "&y=" + y + "&sp=" + getSpeed + "&az=" + getBearing + "&al=" + getAltitude + "&bt=" + bat + "&st=" + satelitySend + "&lg=" + device_idG + "&pw=" + device_idG + "&g=" + percent);

                }

                if (UseSNinsteadOfLogin == false) {
                    httppost = new HttpPost(HttpLink + "?dt=" + getDate + "&x=" + x + "&y=" + y + "&sp=" + getSpeed + "&az=" + getBearing + "&al=" + getAltitude + "&bt=" + bat + "&st=" + satelitySend + "&lg=" + DeviceLogin + "&pw=" + DevicePswd + "&g=" + percent);
                }

                int val;


                Log.d(LOG_TAG, "TimeoutSendDataInSeconds === " + TimeoutSendDataInSeconds);

                try {
                    val = Integer.parseInt(TimeoutSendDataInSeconds);


                    Log.d(LOG_TAG, "нормуль === ");
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                    Log.d(LOG_TAG, "ошибка === ");
                    val = 1;
                }


                Log.d(LOG_TAG, "val === " + val);


                if (val != 0) {


                    try {

                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                        //   nameValuePairs.add(new BasicNameValuePair("lg", DeviceLogin));
                        // nameValuePairs.add(new BasicNameValuePair("pw", DevicePswd));
                        //   nameValuePairs.add(new BasicNameValuePair("y", "111"));
                        // nameValuePairs.add(new BasicNameValuePair("dt", "" + date_now));
                        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        HttpResponse response = httpclient.execute(httppost);

                        Log.d(LOG_TAG, "OK +++");


                        updateDate();
                        openFile(FILENAME);
                        SimpleDateFormat dateFormat6 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                        dateFormat6.setTimeZone(TimeZone.getTimeZone("GMT"));
                        String formattedDateString6 = dateFormat6.format(new Date());

                        saveFile(FILENAME, formattedDateString6 + " SEND post data - OK");


                        checkData = true;

                    } catch (ClientProtocolException e) {
                        // ошибка
                        Log.d(LOG_TAG, "error");
                        updateDate();
                        openFile(FILENAME);
                        SimpleDateFormat dateFormat6 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                        dateFormat6.setTimeZone(TimeZone.getTimeZone("GMT"));
                        String formattedDateString6 = dateFormat6.format(new Date());

                        saveFile(FILENAME, formattedDateString6 + " SEND PING - ERROR");

                        checkData = false;


                    } catch (IOException e) {
                        Log.d(LOG_TAG, "error2");
                        updateDate();
                        openFile(FILENAME);
                        SimpleDateFormat dateFormat6 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                        dateFormat6.setTimeZone(TimeZone.getTimeZone("GMT"));
                        String formattedDateString6 = dateFormat6.format(new Date());

                        saveFile(FILENAME, formattedDateString6 + " SEND post - ERROR");

                        checkData = false;
                        // ошибка
                    }
                }

                // переход на следующую строку
                // а если следующей нет (текущаЯ - последнЯЯ), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(LOG_TAG, "0 rows");
        c.close();


        //clear
        Log.d(LOG_TAG, "--- Clear mytable: ---");
        // удалЯем все записи
        //  int clearCount = db.delete("mytable", null, null);


        //        int delCount = db.delete("mytable", "id = " + id, null);


        //    Log.d(LOG_TAG, "deleted rows count = " + clearCount);

        //   c.close();
        //     dbHelper.close();


        /////
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "--- onCreate database ---");


            updateDate();
            openFile(FILENAME);
            SimpleDateFormat dateFormat6 = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            dateFormat6.setTimeZone(TimeZone.getTimeZone("GMT"));
            String formattedDateString6 = dateFormat6.format(new Date());

            saveFile(FILENAME, formattedDateString6 + " create BD - OK");

            Log.d(LOG_TAG, "--- onCreate database ---");
            // создаем таблицу с полЯми
            db.execSQL("create table mytable ("
                    + "id integer primary key autoincrement,"
                    + "latitude text,"
                    + "longitude text,"
                    + "getBearing text,"
                    + "getSpeed text,"
                    + "getBat text,"
                    + "getAccuracy text,"
                    + "getSat text,"
                    + "getAltitude text,"
                    + "getDate text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    private class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            //  Toast.makeText(getApplicationContext(), "Go to Firstdroid!!! GSM Cinr = "
            //   + String.valueOf(signalStrength.getGsmSignalStrength()), Toast.LENGTH_SHORT).show();
            percent = signalStrength.getGsmSignalStrength();
        }

    }/* End of private Class */


    final Listener onGpsStatusChange = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            Log.e("gps", "in class");
            switch (event) {
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    GpsStatus xGpsStatus = locMgr.getGpsStatus(null);

                    Iterable<GpsSatellite> iSatellites = xGpsStatus.getSatellites();
                    Iterator<GpsSatellite> it = iSatellites.iterator();
                    int count = 0;
                    while (it.hasNext()) {
                        count = count + 1;
                        GpsSatellite oSat = it.next();

                        Log.e("gps", "" + oSat.getSnr());
                    }
                    Log.e("count", "" + count);
                    // tv1.setText(""+count);

                    satelity = count;

                    break;
            }
        }
    };


}


