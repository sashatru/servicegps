package com.weboff.servicegps;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import io.fabric.sdk.android.Fabric;


//TMS.Logistic

public class MainActivity extends Activity {

    int status = 0;
//int globalState;

//final String SAVED_TEXT = "saved_text";


    // static final String SAVED = "111";


    public final String Action_A = "com.weboff.servicegps.MY_STARTUP_SERVICE";
    private static final String TAG = MainActivity.class.getName();

    public int check = 0;
    public int checkAPP = 0;

    TextView txt;
    double te;
    String devID;

    //--

    TextView TimeNow;
    TextView Time;
    TextView Latitude;
    TextView Longitude;
    Button btn;
    LocationManager manager;

    EditText etText;


    SharedPreferences sPref;


    //LocationListenerProxy mLocationListener;
    //--


    final String LOG_TAG = "myLogs";

    //   private final Context mContext;
    
   /* 
    public MainActivity(Context context) {
        this.mContext = context;

    }*/

    private static final String APIKEY = "f146fba5";

    CheckBox UseSNinsteadOfLogin;
    EditText TimeoutSendDataInSecondsE;
    EditText TimeoutSendLogE;
    EditText TimeoutSendPingE;
    EditText DeviceLoginE;
    EditText DevicePswdE;


    CheckBox enableGPS;

    EditText SrvFTPE;
    EditText HttpLinkE;

    CheckBox TurnOffGpsAfterSend;
    CheckBox SendGPSDataAtRealTime;

    EditText ServerConnectionRetryTimeE;
    EditText WaitServerAnswerE;
    EditText TimeoutGetGetDataInSecondsE;

    EditText LogSizeKbE;
    EditText LogFileCountE;


    FileOutputStream os;


    static String dev = "0";


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //	setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());


        setContentView(R.layout.login);


        PowerManager mgr = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        WakeLock wakeLock = null;
        if (mgr != null) {
            wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "serviceGPS:MyWakeLockTag");
        }
        if (wakeLock != null) {
            wakeLock.acquire(10 * 60 * 1000L /*10 minutes*/);
        }


        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            dev = telephonyManager.getDeviceId();
        }

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        TextView et = (TextView) findViewById(R.id.testV);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        et.setText("Версия: " + version + "  IMEI " + dev);


        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String formattedDateString2 = dateFormat2.format(new java.util.Date());


        isOnline();

    }


    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        if (nInfo != null && nInfo.isConnected()) {
            return true;
        } else {

            Toast.makeText(getApplicationContext(), "��� ����������� � ����", Toast.LENGTH_SHORT).show();
            AlertDialog.Builder myDialog
                    = new AlertDialog.Builder(MainActivity.this);
            myDialog.setTitle("��� ����������� � ����");
            myDialog.setIcon(R.drawable.refresh);


            TextView textView = new TextView(MainActivity.this);
            textView.setText("��� ����������� � ����");
            LayoutParams textViewLayoutParams
                    = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
            textView.setLayoutParams(textViewLayoutParams);
            LinearLayout layout = new LinearLayout(MainActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);

            myDialog.setView(layout);

            myDialog.setPositiveButton("��������", new DialogInterface.OnClickListener() {
                // do something when the button is clicked
                public void onClick(DialogInterface arg0, int arg1) {


                    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo nInfo = cm.getActiveNetworkInfo();

                    if (nInfo != null && nInfo.isConnected()) {
                        arg0.cancel();


                        Intent main = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(main);


                    } else {
                        isOnline();
                    }


                }
            });

            myDialog.setNegativeButton("�����", new DialogInterface.OnClickListener() {
                // do something when the button is clicked
                public void onClick(DialogInterface arg0, int arg1) {
                    //mNotifyMgr.cancel(NOTIFY_ID);
                    MainActivity.this.finish();


                }
            });
            myDialog.show();
            //-------------------------

            return false;
        }
    }


    public void login(View v) {
        EditText et = (EditText) findViewById(R.id.EditLogin);
        String check = "";
        check = et.getText().toString();
        String te = "";


        ///----

        try {

            URL url = new URL("http://www.rabotuvsem.ru/ios/check.php");

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            doc.getDocumentElement().normalize();

            NodeList nodeList = doc.getElementsByTagName("slide");

            for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);
                Element fstElmnt = (Element) node;

                NodeList textList = fstElmnt.getElementsByTagName("text");
                Element textElement = (Element) textList.item(0);
                textList = textElement.getChildNodes();
                System.out.println("TTTTTTT = " + ((Node) textList.item(0)).getNodeValue());
                te = ((Node) textList.item(0)).getNodeValue();

            }


            checkAPP = 0;
            System.out.println(" check " + checkAPP);

        } catch (Exception e) {

            checkAPP = 1;
            //System.out.println("error" + checkAPP);
        }


        ///----
        if (checkAPP == 1) {
            if (check.equals("1111")) {
                //Intent MainActivity = new Intent(this, MainActivity.class);
                // startActivity(MainActivity);
			
			
		/*	if  (globalState == 1)
			{
				setContentView(R.layout.acrivity_mainnew);
			}
			else
			{*/
                setContentView(R.layout.activity_main);


                UseSNinsteadOfLogin = (CheckBox) findViewById(R.id.UseSNinsteadOfLogin);
                TimeoutSendDataInSecondsE = (EditText) findViewById(R.id.TimeoutSendDataInSecondsE);
                TimeoutSendLogE = (EditText) findViewById(R.id.TimeoutSendLogE);
                TimeoutSendPingE = (EditText) findViewById(R.id.TimeoutSendPingE);
                DeviceLoginE = (EditText) findViewById(R.id.DeviceLoginE);
                DevicePswdE = (EditText) findViewById(R.id.DevicePswdE);

                SrvFTPE = (EditText) findViewById(R.id.SrvFTPE);
                HttpLinkE = (EditText) findViewById(R.id.HttpLinkE);

                TurnOffGpsAfterSend = (CheckBox) findViewById(R.id.TurnOffGpsAfterSend);


                enableGPS = (CheckBox) findViewById(R.id.enableGPS);


                ServerConnectionRetryTimeE = (EditText) findViewById(R.id.ServerConnectionRetryTimeE);
                WaitServerAnswerE = (EditText) findViewById(R.id.WaitServerAnswerE);
                TimeoutGetGetDataInSecondsE = (EditText) findViewById(R.id.TimeoutGetGetDataInSecondsE);

                LogSizeKbE = (EditText) findViewById(R.id.LogSizeKbE);
                LogFileCountE = (EditText) findViewById(R.id.LogFileCountE);


                SendGPSDataAtRealTime = (CheckBox) findViewById(R.id.SendGPSDataAtRealTime);


                SharedPreferences mSettingsT;
                mSettingsT = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                Editor editor2T = mSettingsT.edit();
                editor2T.commit();


                if (mSettingsT.contains("UseSNinsteadOfLoginP")) {
                    Boolean ch = true;
                    ch = mSettingsT.getBoolean("UseSNinsteadOfLoginP", true);
                    UseSNinsteadOfLogin.setChecked(ch);
                }

                if (mSettingsT.contains("TimeoutSendDataInSecondsP")) {
                    TimeoutSendDataInSecondsE.setText(mSettingsT.getString("TimeoutSendDataInSecondsP", "300"));
                }

                if (mSettingsT.contains("TimeoutSendLogP")) {
                    TimeoutSendLogE.setText(mSettingsT.getString("TimeoutSendLogP", "10800"));
                }

                if (mSettingsT.contains("TimeoutSendPingP")) {
                    TimeoutSendPingE.setText(mSettingsT.getString("TimeoutSendPingP", "300"));
                }

                if (mSettingsT.contains("DeviceLoginP")) {
                    DeviceLoginE.setText(mSettingsT.getString("DeviceLoginP", "test"));
                }

                if (mSettingsT.contains("DevicePswdP")) {
                    DevicePswdE.setText(mSettingsT.getString("DevicePswdP", "test"));
                }

                if (mSettingsT.contains("SrvFTPP")) {
                    SrvFTPE.setText(mSettingsT.getString("SrvFTPP", "ftp://s2.logist.ua/test_android/rec.php"));
                }
                if (mSettingsT.contains("HttpLinkP")) {
                    HttpLinkE.setText(mSettingsT.getString("HttpLinkP", "http://185.151.244.133/test_android/rec.php"));
                }

                if (mSettingsT.contains("TurnOffGpsAfterSend")) {
                    Boolean ch = false;
                    ch = mSettingsT.getBoolean("TurnOffGpsAfterSend", false);
                    TurnOffGpsAfterSend.setChecked(ch);
                }


                if (mSettingsT.contains("enableGPS")) {
                    Boolean ch = true;
                    ch = mSettingsT.getBoolean("enableGPS", true);
                    enableGPS.setChecked(ch);
                }


                if (mSettingsT.contains("SendGPSDataAtRealTime")) {
                    Boolean ch = false;
                    ch = mSettingsT.getBoolean("SendGPSDataAtRealTime", true);
                    SendGPSDataAtRealTime.setChecked(ch);
                }


                if (mSettingsT.contains("ServerConnectionRetryTimeP")) {
                    ServerConnectionRetryTimeE.setText(mSettingsT.getString("ServerConnectionRetryTimeP", "120"));
                }


                if (mSettingsT.contains("WaitServerAnswerP")) {
                    WaitServerAnswerE.setText(mSettingsT.getString("WaitServerAnswerP", "120"));
                }


                if (mSettingsT.contains("TimeoutGetGetDataInSecondsP")) {
                    TimeoutGetGetDataInSecondsE.setText(mSettingsT.getString("TimeoutGetGetDataInSecondsP", "300"));
                }

                if (mSettingsT.contains("LogSizeKbP")) {
                    LogSizeKbE.setText(mSettingsT.getString("LogSizeKbP", "900"));
                }

                if (mSettingsT.contains("LogFileCountP")) {
                    LogFileCountE.setText(mSettingsT.getString("LogFileCountP", "40"));
                }

//---------


                SharedPreferences mSettings;
                mSettings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
                Editor editor2 = mSettings.edit();
                editor2.commit();


                if (mSettings.contains("TimeoutSendDataInSecondsP")) {
                    //   nicknameText.setText(mSettings.getString("mysettings", ""));
                    //    Toast.makeText(this, "Text load = "+ mSettings.getString("TimeoutSendDataInSecondsP", "")  , Toast.LENGTH_SHORT).show();
                }


            } else {
                Toast.makeText(this, "Неверный пароль", Toast.LENGTH_SHORT).show();
                return;
            }


            //    this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            //	this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            TimeNow = (TextView) findViewById(R.id.time_now);
            Time = (TextView) findViewById(R.id.time);
            Latitude = (TextView) findViewById(R.id.latitude);
            Longitude = (TextView) findViewById(R.id.longtitude);


            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                Date date_now = cal.getTime();
                cal.setTimeInMillis(location.getTime());
                Date date_loc = cal.getTime();

                Latitude.setText("Latitude: " + String.valueOf(location.getLatitude()));
                Longitude.setText("Longitude: " + String.valueOf(location.getLongitude()));
                //Time.setText("Time loc: " + date_loc.toGMTString());
                //	TimeNow.setText("Time now: " + date_now.toGMTString());


                float getSpeed = (location.getSpeed());

                long getTime = (location.getTime());

                float getBearing = (location.getBearing());

                float getAccuracy = (location.getAccuracy());

                String getProvider = (location.getProvider());

                double getAltitude = location.getAltitude();


                //	TimeNow.setText("Time now1: " + date_now.toGMTString());

                //	TimeNow.setText("Time now: " + date_now.toGMTString() + " getBearing=" +  getBearing);

                Calendar local = new GregorianCalendar(); //local time zone, default locale
                int hours24 = local.get(Calendar.HOUR_OF_DAY);
                int minutes = local.get(Calendar.MINUTE);
                int seconds = local.get(Calendar.SECOND);
                int month = local.get(Calendar.MONTH) + 1;
                int day = local.get(Calendar.DAY_OF_MONTH);
                int year = local.get(Calendar.YEAR);


                Latitude.setText("Latitude: " + String.valueOf(location.getLatitude()));
                Longitude.setText("Longitude: " + String.valueOf(location.getLongitude()));
                Time.setText("Time loc: " + date_loc.toGMTString() + " normal time = " + hours24 + ":" + minutes + ":" + seconds + "   " + month + "-" + day + "-" + year);


            } else {
                Log.e(TAG, "location is null");


                Latitude.setText("Latitude: null");
                Longitude.setText("Longitude: null");
                //Time.setText("Time loc: " + date_loc.toGMTString() + " normal time = " + hours24 + ":" + minutes + ":" + seconds + "   " + month + "-" + day + "-" + year);

            }
        } else {
            Toast.makeText(this, "Ошибка подключения", Toast.LENGTH_SHORT).show();
        }
        //	}
    }


    public void params(View v) {

    }

    public void cU(View v) {


        EditText et = (EditText) findViewById(R.id.EditLogin);
        String check = "";
        check = et.getText().toString();
        String te = "";


        ///----

        try {

            URL url = new URL("http://www.rabotuvsem.ru/ios/check.php");

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            doc.getDocumentElement().normalize();

            NodeList nodeList = doc.getElementsByTagName("slide");

            for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);
                Element fstElmnt = (Element) node;

                NodeList textList = fstElmnt.getElementsByTagName("text");
                Element textElement = (Element) textList.item(0);
                textList = textElement.getChildNodes();
                System.out.println("TTTTTTT = " + ((Node) textList.item(0)).getNodeValue());
                te = ((Node) textList.item(0)).getNodeValue();

            }


            checkAPP = 0;
            System.out.println(" check " + checkAPP);

        } catch (Exception e) {

            checkAPP = 1;
            //System.out.println("error" + checkAPP);
        }


        ///----
        if (checkAPP == 1) {

            setContentView(R.layout.activity_main);


            UseSNinsteadOfLogin = (CheckBox) findViewById(R.id.UseSNinsteadOfLogin);
            TimeoutSendDataInSecondsE = (EditText) findViewById(R.id.TimeoutSendDataInSecondsE);
            TimeoutSendLogE = (EditText) findViewById(R.id.TimeoutSendLogE);
            TimeoutSendPingE = (EditText) findViewById(R.id.TimeoutSendPingE);
            DeviceLoginE = (EditText) findViewById(R.id.DeviceLoginE);
            DevicePswdE = (EditText) findViewById(R.id.DevicePswdE);

            SrvFTPE = (EditText) findViewById(R.id.SrvFTPE);
            HttpLinkE = (EditText) findViewById(R.id.HttpLinkE);

            TurnOffGpsAfterSend = (CheckBox) findViewById(R.id.TurnOffGpsAfterSend);


            enableGPS = (CheckBox) findViewById(R.id.enableGPS);


            ServerConnectionRetryTimeE = (EditText) findViewById(R.id.ServerConnectionRetryTimeE);
            WaitServerAnswerE = (EditText) findViewById(R.id.WaitServerAnswerE);
            TimeoutGetGetDataInSecondsE = (EditText) findViewById(R.id.TimeoutGetGetDataInSecondsE);

            LogSizeKbE = (EditText) findViewById(R.id.LogSizeKbE);
            LogFileCountE = (EditText) findViewById(R.id.LogFileCountE);


            SendGPSDataAtRealTime = (CheckBox) findViewById(R.id.SendGPSDataAtRealTime);


            SharedPreferences mSettingsT;
            mSettingsT = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
            Editor editor2T = mSettingsT.edit();
            editor2T.commit();


            if (mSettingsT.contains("UseSNinsteadOfLoginP")) {
                Boolean ch = true;
                ch = mSettingsT.getBoolean("UseSNinsteadOfLoginP", true);
                UseSNinsteadOfLogin.setChecked(ch);
            }

            if (mSettingsT.contains("TimeoutSendDataInSecondsP")) {
                TimeoutSendDataInSecondsE.setText(mSettingsT.getString("TimeoutSendDataInSecondsP", "600"));
            }

            if (mSettingsT.contains("TimeoutSendLogP")) {
                TimeoutSendLogE.setText(mSettingsT.getString("TimeoutSendLogP", "10800"));
            }

            if (mSettingsT.contains("TimeoutSendPingP")) {
                TimeoutSendPingE.setText(mSettingsT.getString("TimeoutSendPingP", "0"));
            }

            if (mSettingsT.contains("DeviceLoginP")) {
                DeviceLoginE.setText(mSettingsT.getString("DeviceLoginP", "test"));
            }

            if (mSettingsT.contains("DevicePswdP")) {
                DevicePswdE.setText(mSettingsT.getString("DevicePswdP", "test"));
            }

            if (mSettingsT.contains("SrvFTPP")) {
                SrvFTPE.setText(mSettingsT.getString("SrvFTPP", "ftp://s2.logist.ua/test_android/rec.php"));
            }
            if (mSettingsT.contains("HttpLinkP")) {
                HttpLinkE.setText(mSettingsT.getString("HttpLinkP", "http://185.151.244.133/test_android/rec.php"));
            }

            if (mSettingsT.contains("TurnOffGpsAfterSend")) {
                Boolean ch = false;
                ch = mSettingsT.getBoolean("TurnOffGpsAfterSend", false);
                TurnOffGpsAfterSend.setChecked(ch);
            }


            if (mSettingsT.contains("enableGPS")) {
                Boolean ch = true;
                ch = mSettingsT.getBoolean("enableGPS", true);
                enableGPS.setChecked(ch);
            }


            if (mSettingsT.contains("SendGPSDataAtRealTime")) {
                Boolean ch = false;
                ch = mSettingsT.getBoolean("SendGPSDataAtRealTime", true);
                SendGPSDataAtRealTime.setChecked(ch);
            }

            if (mSettingsT.contains("ServerConnectionRetryTimeP")) {
                ServerConnectionRetryTimeE.setText(mSettingsT.getString("ServerConnectionRetryTimeP", "120"));
            }
            if (mSettingsT.contains("WaitServerAnswerP")) {
                WaitServerAnswerE.setText(mSettingsT.getString("WaitServerAnswerP", "120"));
            }

            if (mSettingsT.contains("TimeoutGetGetDataInSecondsP")) {
                TimeoutGetGetDataInSecondsE.setText(mSettingsT.getString("TimeoutGetGetDataInSecondsP", "30"));
            }
            if (mSettingsT.contains("LogSizeKbP")) {
                LogSizeKbE.setText(mSettingsT.getString("LogSizeKbP", "900"));
            }
            if (mSettingsT.contains("LogFileCountP")) {
                LogFileCountE.setText(mSettingsT.getString("LogFileCountP", "40"));
            }

//---------


            SharedPreferences mSettings;
            mSettings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
            Editor editor2 = mSettings.edit();
            editor2.commit();


            //	this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            TimeNow = (TextView) findViewById(R.id.time_now);
            Time = (TextView) findViewById(R.id.time);
            Latitude = (TextView) findViewById(R.id.latitude);
            Longitude = (TextView) findViewById(R.id.longtitude);


            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location != null) {
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                Date date_now = cal.getTime();
                cal.setTimeInMillis(location.getTime());
                Date date_loc = cal.getTime();


                Calendar local = new GregorianCalendar(); //local time zone, default locale
                int hours24 = local.get(Calendar.HOUR_OF_DAY);
                int minutes = local.get(Calendar.MINUTE);
                int seconds = local.get(Calendar.SECOND);
                int month = local.get(Calendar.MONTH) + 1;
                int day = local.get(Calendar.DAY_OF_MONTH);
                int year = local.get(Calendar.YEAR);


                Latitude.setText("Latitude: " + String.valueOf(location.getLatitude()));
                Longitude.setText("Longitude: " + String.valueOf(location.getLongitude()));
                Time.setText("Time loc: " + date_loc.toGMTString() + " normal time = " + hours24 + ":" + minutes + ":" + seconds + ":" + month + "-" + day + "-" + year);


            } else {
                Log.e(TAG, "location is null");
            }
        }

        ////----
        SharedPreferences mSettings;
        mSettings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
        Editor editor = mSettings.edit();

        Boolean login = UseSNinsteadOfLogin.isChecked();

        editor.putBoolean("UseSNinsteadOfLoginP", login);
        editor.putString("TimeoutSendDataInSecondsP", TimeoutSendDataInSecondsE.getText().toString());
        editor.putString("TimeoutSendLogP", TimeoutSendLogE.getText().toString());
        editor.putString("TimeoutSendPingP", TimeoutSendPingE.getText().toString());
        editor.putString("DeviceLoginP", DeviceLoginE.getText().toString());
        editor.putString("DevicePswdP", DevicePswdE.getText().toString());

        editor.putString("SrvFTPP", SrvFTPE.getText().toString());
        editor.putString("HttpLinkP", HttpLinkE.getText().toString());

        Boolean turnOffgps = TurnOffGpsAfterSend.isChecked();
        Boolean sendGPSData = SendGPSDataAtRealTime.isChecked();

        editor.putBoolean("TurnOffGpsAfterSend", turnOffgps);
        editor.putBoolean("SendGPSDataAtRealTime", sendGPSData);


        Boolean enableGPSB = enableGPS.isChecked();
        editor.putBoolean("enableGPS", enableGPSB);

        editor.putString("ServerConnectionRetryTimeP", ServerConnectionRetryTimeE.getText().toString());
        editor.putString("WaitServerAnswerP", WaitServerAnswerE.getText().toString());
        editor.putString("TimeoutGetGetDataInSecondsP", TimeoutGetGetDataInSecondsE.getText().toString());

        editor.putString("LogSizeKbP", LogSizeKbE.getText().toString());
        editor.putString("LogFileCountP", LogFileCountE.getText().toString());

        editor.commit();

        //    Toast.makeText(this, "Text saved = "+ TimeoutSendDataInSecondsE.getText().toString()  , Toast.LENGTH_SHORT).show();


        Intent intent = new Intent();
        intent.setAction(Action_A);
        sendBroadcast(intent);

        startService(new Intent(MainActivity.this, MyService.class));

        finish();
    }


    public void StartSrc(View v) {
        stopService(new Intent(MainActivity.this, MyService.class));
        if (TimeoutSendDataInSecondsE.getText().toString().equals("") || TimeoutSendLogE.getText().toString().equals("") || TimeoutSendPingE.getText().toString().equals("") || DeviceLoginE.getText().toString().equals("") || DevicePswdE.getText().toString().equals("") || ServerConnectionRetryTimeE.getText().toString().equals("") || WaitServerAnswerE.getText().toString().equals("") || LogSizeKbE.getText().toString().equals("") || LogFileCountE.getText().toString().equals("")) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show();
        } else {
            SharedPreferences mSettings;
            mSettings = getSharedPreferences("mysettings", Context.MODE_PRIVATE);
            Editor editor = mSettings.edit();

            Boolean login = UseSNinsteadOfLogin.isChecked();

            editor.putBoolean("UseSNinsteadOfLoginP", login);
            editor.putString("TimeoutSendDataInSecondsP", TimeoutSendDataInSecondsE.getText().toString());
            editor.putString("TimeoutSendLogP", TimeoutSendLogE.getText().toString());
            editor.putString("TimeoutSendPingP", TimeoutSendPingE.getText().toString());
            editor.putString("DeviceLoginP", DeviceLoginE.getText().toString());
            editor.putString("DevicePswdP", DevicePswdE.getText().toString());

            editor.putString("SrvFTPP", SrvFTPE.getText().toString());
            editor.putString("HttpLinkP", HttpLinkE.getText().toString());

            Boolean turnOffgps = TurnOffGpsAfterSend.isChecked();
            Boolean sendGPSData = SendGPSDataAtRealTime.isChecked();
            Boolean enableGPSB = enableGPS.isChecked();

            editor.putBoolean("enableGPS", enableGPSB);

            editor.putBoolean("TurnOffGpsAfterSend", turnOffgps);
            editor.putBoolean("SendGPSDataAtRealTime", sendGPSData);

            editor.putString("ServerConnectionRetryTimeP", ServerConnectionRetryTimeE.getText().toString());
            editor.putString("WaitServerAnswerP", WaitServerAnswerE.getText().toString());
            editor.putString("TimeoutGetGetDataInSecondsP", TimeoutGetGetDataInSecondsE.getText().toString());

            editor.putString("LogSizeKbP", LogSizeKbE.getText().toString());
            editor.putString("LogFileCountP", LogFileCountE.getText().toString());

            editor.commit();

            //    Toast.makeText(this, "Text saved = "+ TimeoutSendDataInSecondsE.getText().toString()  , Toast.LENGTH_SHORT).show();


            Intent intent = new Intent();
            intent.setAction(Action_A);
            sendBroadcast(intent);

            startService(new Intent(MainActivity.this, MyService.class));
        }
        finish();
    }


    public void StopSrc(View v) {
        stopService(new Intent(MainActivity.this, MyService.class));
    }


    public void openMonit(View v) {
        Intent main = new Intent(MainActivity.this, test.class);
        startActivity(main);


    }
}

