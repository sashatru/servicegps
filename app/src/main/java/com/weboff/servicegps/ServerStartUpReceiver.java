package com.weboff.servicegps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ServerStartUpReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
        Log.d("myServiceLogs", "ServerStartUpReceiver onReceive() intent = " + intent);
		Intent service = new Intent(context, MyService.class);
		context.startService(service);
	}
}
 