package com.weboff.servicegps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class MyLocation implements LocationListener {

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private Location myLocation, gpsLocation, mLastLocationFetched, newerLocation;
    private LocationManager locationManager;
    private Context context;
    private static final float MAX_ACCURACY = 60.0f;
    private static final float MAX_ACCELERATION = 3.93f; //m/(s*s) 170 km/h for 12 sec.
    private static final float MAX_ACCEPTED_SPEED = 60.f; //m/s  216 km/h

    public MyLocation(Context context) {
        this.context = context;
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        Log.i("MyLocationCOORDS", "MyLocation init");
    }

    private void setLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @SuppressLint("MissingPermission")
    public void startPreparations(){
        Log.i("MyLocationCOORDS", "startPreparations()");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location != null){
                        myLocation = location;
                    }
                }
            });
            setLocationRequest();
            setLocationCallback();

            fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                    locationCallback, null /* Looper */);
    }

    private void setLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    myLocation = location;
                }
            }
        };
    }

    public Location getActualLocation(){
        if (isGpsEnabled()) {
            try {
                gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            } catch (SecurityException e) {}
            Log.i("MyLocationCOORDS", "gpsLocation = " + gpsLocation);
        }


        if (gpsLocation == null && myLocation==null && mLastLocationFetched == null) {
            Log.w("MyLocationCOORDS", "location = null");
            return null;
        }

        if (gpsLocation!=null) {gpsLocation = gpsLocation.getAccuracy()<=MAX_ACCURACY?gpsLocation:null;}
        //if (googleLocation!=null){googleLocation = googleLocation.getAccuracy()<=MAX_ACCURACY?googleLocation:null;}

        if (gpsLocation!=null&&myLocation==null){newerLocation = gpsLocation;}
        if (gpsLocation ==null&&myLocation!=null){newerLocation = myLocation;}
        if (gpsLocation!=null&&myLocation!=null){
            newerLocation = gpsLocation.getTime()>=myLocation.getTime()?
                    gpsLocation:myLocation;
        }

        if (mLastLocationFetched == null && newerLocation != null) {
            mLastLocationFetched = newerLocation;
            Log.w("MyLocationCOORDS", "#1");
            return mLastLocationFetched;
        } else {
            Log.w("MyLocationCOORDS", "#4");
            try {
                mLastLocationFetched = getBetterLocation(newerLocation, mLastLocationFetched);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.w("MyLocationCOORDS", "#41");
        return mLastLocationFetched;
    }

    public boolean isGpsEnabled(){
        return locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER );
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }



    /**
     * Determines whether one of new Locations reading is better than the current Location fix
     *
     * @param currentLocation The new Location that you want to evaluate
     * @param lastLocation    The current Location fix, to which you want to compare the new one
     */
    protected static Location getBetterLocation(Location currentLocation, Location lastLocation) {
        Log.w("MyLocationCOORDS", "getBetterLocation:\ncurrentLocation = "+currentLocation+"\nlastLocation = "+lastLocation);
        float timeIntervalByCoords =  ((float)(currentLocation.getTime() - lastLocation.getTime()) / 1.0E3F);
        Log.w("MyLocationCOORDS", "timeIntervalByCoords = " + timeIntervalByCoords);
        float maxPossibleSpeedSinceLastCoordFixed = ((lastLocation.getSpeed() + MAX_ACCELERATION * timeIntervalByCoords) < MAX_ACCEPTED_SPEED ? (lastLocation.getSpeed() + MAX_ACCELERATION * timeIntervalByCoords) : MAX_ACCEPTED_SPEED);
        Log.w("MyLocationCOORDS", "MAX_speed = " + maxPossibleSpeedSinceLastCoordFixed);
        //float maxPossiblePathSinceLastCoordFixed = (lastLocation.getSpeed() * timeIntervalByCoords + ((MAX_ACCELERATION * timeIntervalByCoords * timeIntervalByCoords) / 2));
        float maxPossiblePathSinceLastCoordFixed = maxPossibleSpeedSinceLastCoordFixed < MAX_ACCEPTED_SPEED ? lastLocation.getSpeed() * timeIntervalByCoords + ((MAX_ACCELERATION * timeIntervalByCoords * timeIntervalByCoords) / 2): (458.0f+MAX_ACCEPTED_SPEED*(timeIntervalByCoords-15.27f));
        Log.w("MyLocationCOORDS", "MAX_distance = " + maxPossiblePathSinceLastCoordFixed);
        float distanceByCoords = currentLocation.distanceTo(lastLocation);
        Log.w("MyLocationCOORDS", "distanceByCoords = " + distanceByCoords);
        float speedByCoords = (float) (distanceByCoords == 0.0 ? 0.0 : (distanceByCoords / timeIntervalByCoords));

        if (distanceByCoords <= maxPossiblePathSinceLastCoordFixed
                && speedByCoords <= maxPossibleSpeedSinceLastCoordFixed) {
            return currentLocation;
        } else
            return lastLocation;
    }
}